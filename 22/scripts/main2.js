
document.addEventListener("DOMContentLoaded", function(e) {
  elements = document.getElementsByClassName("firstLevel");
  for (var i = 0; i < elements.length; i++){
    elements[i].onclick = function(event){
      if (event.target.getAttribute('class') != "firstLevel"){
        return;
      }
      list = event.target.getElementsByClassName("inner")[0];
      if (list.style.display == "" || list.style.display == "block"){
        list.style.display = "none";
      }
      else{
        list.style.display = "block";
      }
    }
  }
})
